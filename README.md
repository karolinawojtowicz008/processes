# Processes

Processes tests and reports in html table execution time of an algorythm in different conditions: 
- CPU
- One thread
- Multiple threads
- One process
- Multiple processes

## Python version

Processes uses Python version 3. 8. 3.


## Running the script

```sh
# run a raport file from terminal
$ open raport_htmml.html
# or
$ PYTHONPATH=. raport_htmml.html run
```


#### Running tests 

see : http://doc.pytest.org/en/latest/capture.html
```sh
$ PYTHONPATH=. py.test
$ PYTHONPATH=. py.test --verbose -s
```


## Installation


```sh
#check python version
$ python --version
$ pip install multiprocess
```

## Structure
```sh
├── master
│   ├── README.md
│   ├── .gitignore
├── main
│   ├── cpu_process.py
│   ├── dane.py
│   ├── main.py
│   ├── mt_process.py
│   ├── mt_thread.py
│   └── one_thread.py
│   └── raport.py
│   └── thread_lock.py
└── 
```
## The execution of the processes (example - cpu process)
Each code file uses muliprocessing library and time module for measuring execution time for every process. 
```sh
#imoprted libraries
from multiprocessing import Pool
import time
import os
from statistics import median
```
Function 'count' is a main part of the code where the algorythm for later declared numbers is being run. The function is also making a list where is stores saved time of of each number.
```sh
#function initiation
def count(numbers):

    time_list = [] #creating an empty list fot time results storage
    time_tmp = 0 #creating a variable
    for runs in range(5): 
        start = time.time() #setting a variable for a beggining of time measuring
        for number in numbers: #looping the algorythm for each number from the list
            add = 0 #setting variables
            duration = 0
            for i in range(0, number + 1): #actual algorythm execution 
                add += i
            print(add)
        duration = time.time() - start #saving end time as a variable
        print(duration)
        time_list.append(duration) #adding resulted time to the list 

    print(time_list) 
    time_med_4 = round(median(time_list), 3) #counting a time median and rounding
    print(time_med_4)

#here the function is saving results into another file for later use and comparison
    r = open("dane.py", "a")
    r.write("\ntime_4 = ")
    r.writelines(str(time_list))
    r.write("\ntime_med_4 = ")
    r.writelines(str(time_med_4))
    r.close()
```

Function 'main' is a declaration of the numbers and an invocation of the processes

```sh
#function initiation
def main():
# declaring a list of numbers for the algorythm in count function
    numbers = [15972490, 80247910, 92031257, 75940266, 97986012, 87599664, 75231321,
               11138524, 68870499, 11872796, 79132533, 40649382,
               63886074, 53146293, 36914087, 62770938]

#reading and storing number of cpu cores of the computer
    cpu = os.cpu_count() 
#invocation of the processes using function of the multiprocessing module running it with the number of processes equal to cpu number 
    with Pool(processes=cpu) as pool:
        pool.map(count, [numbers])

#function call
if __name__ == '__main__':
    main()

```
Processes program is storing all the data combined in the file dane.py
Every used method has it's own time list and madian.
Example:
```sh
time_1 = [91.599, 88.383, 129.752, 189.704, 195.719]
time_med_1 = 129.752
time_3 = [159.752, 223.684, 131.366, 104.943, 86.927]
time_med_3 = 131.366
time_2 = [229.502, 133.125, 105.774, 87.716, 84.93]
time_med_2 = 105.774
time_4 = [84.19170594215393, 87.77338314056396]
time_med_4 = 85.983
```
File raport.py is using the data to create an html file for a full raport in the table 

